var class_galactic_factories_1_1_data_1_1_factories =
[
    [ "Factories", "class_galactic_factories_1_1_data_1_1_factories.html#af5ade1bc7d81590c173346673418d844", null ],
    [ "FactoryName", "class_galactic_factories_1_1_data_1_1_factories.html#ae90825b3aac0cf5e2640b346db626ada", null ],
    [ "ID", "class_galactic_factories_1_1_data_1_1_factories.html#a328fce8eb8920091cdcce125f1f57bf7", null ],
    [ "Leader", "class_galactic_factories_1_1_data_1_1_factories.html#a443526858d1cb9571e0df1bca0f0b23f", null ],
    [ "Models", "class_galactic_factories_1_1_data_1_1_factories.html#ab4823c030b5d24b422a5db9af5efc9a6", null ],
    [ "Planet", "class_galactic_factories_1_1_data_1_1_factories.html#a4caa796ea68514aadd6dff2965a3ebf6", null ],
    [ "Side", "class_galactic_factories_1_1_data_1_1_factories.html#a8873bbcbca0b486865d4bd054368666d", null ],
    [ "StrategicValue", "class_galactic_factories_1_1_data_1_1_factories.html#af9e069f6775b451c1129356ae961dd3f", null ]
];