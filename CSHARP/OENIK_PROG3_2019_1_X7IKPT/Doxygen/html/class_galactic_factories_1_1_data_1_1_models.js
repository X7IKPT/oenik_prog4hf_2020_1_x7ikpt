var class_galactic_factories_1_1_data_1_1_models =
[
    [ "Models", "class_galactic_factories_1_1_data_1_1_models.html#a46f956852cbb80286557220fd556f977", null ],
    [ "Color", "class_galactic_factories_1_1_data_1_1_models.html#aa6eeb37a6cbebc9fcf2e374e28595a65", null ],
    [ "Factories", "class_galactic_factories_1_1_data_1_1_models.html#adbe34ed589514eb0ee564fd1c6745c3f", null ],
    [ "FactoryID", "class_galactic_factories_1_1_data_1_1_models.html#acad9d6c1695022811484cb634f3687f6", null ],
    [ "FirePower", "class_galactic_factories_1_1_data_1_1_models.html#aaf8861b0b7993f21143d88f0902256c1", null ],
    [ "ID", "class_galactic_factories_1_1_data_1_1_models.html#aabeb481b3c19b76b9b4c195305529362", null ],
    [ "ModelName", "class_galactic_factories_1_1_data_1_1_models.html#a0da08b7f57ca36c15e92e4677da51f19", null ],
    [ "ModelUpgradeSwitch", "class_galactic_factories_1_1_data_1_1_models.html#a63fd757a80a991e85413b58a70c465f4", null ],
    [ "Price", "class_galactic_factories_1_1_data_1_1_models.html#add669928996faec301457cb9f79af3d3", null ],
    [ "RequiresCrew", "class_galactic_factories_1_1_data_1_1_models.html#a2fdbfca9e389c32aa2cb98cbfff70640", null ]
];