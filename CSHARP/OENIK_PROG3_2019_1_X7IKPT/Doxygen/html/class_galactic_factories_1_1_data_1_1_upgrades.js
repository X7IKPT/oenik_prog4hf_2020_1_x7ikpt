var class_galactic_factories_1_1_data_1_1_upgrades =
[
    [ "Upgrades", "class_galactic_factories_1_1_data_1_1_upgrades.html#af05883b213a30f86353952a8f91bbac1", null ],
    [ "FirePowerIncrease", "class_galactic_factories_1_1_data_1_1_upgrades.html#ad0c6ac446d4cec22946485fe071418d8", null ],
    [ "ID", "class_galactic_factories_1_1_data_1_1_upgrades.html#a836fbcf10c77e5850ea6217ddecacf2d", null ],
    [ "ModelUpgradeSwitch", "class_galactic_factories_1_1_data_1_1_upgrades.html#a345bc7c04a5077c5f392fc930a34e33e", null ],
    [ "Price", "class_galactic_factories_1_1_data_1_1_upgrades.html#ae058f4a19544c34ccb9d973efc4f214d", null ],
    [ "Reuseable", "class_galactic_factories_1_1_data_1_1_upgrades.html#a0e13def0da130af28d8920a655f2f33a", null ],
    [ "UpgradeName", "class_galactic_factories_1_1_data_1_1_upgrades.html#a500c2dc2d39784e65d8a9dd8957f37f4", null ],
    [ "UpgradeType", "class_galactic_factories_1_1_data_1_1_upgrades.html#a3b9182c651262be6940dee8fa7ae8b7a", null ]
];