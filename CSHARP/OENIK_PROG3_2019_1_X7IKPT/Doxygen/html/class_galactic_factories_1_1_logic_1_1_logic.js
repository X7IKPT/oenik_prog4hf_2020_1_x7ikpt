var class_galactic_factories_1_1_logic_1_1_logic =
[
    [ "Logic", "class_galactic_factories_1_1_logic_1_1_logic.html#a9c97164bd3cf0071ebc3525a84911ae3", null ],
    [ "AveragePriceofFactory", "class_galactic_factories_1_1_logic_1_1_logic.html#ab1f80af626aed887391ef0ec538f4a76", null ],
    [ "FactoriesGetAll", "class_galactic_factories_1_1_logic_1_1_logic.html#a6f9bb3b0929dc7e7c8fe03b660fde131", null ],
    [ "FactoryCreate", "class_galactic_factories_1_1_logic_1_1_logic.html#a97d08f59b5e72e2bc490f27d37310c7f", null ],
    [ "FactoryDelete", "class_galactic_factories_1_1_logic_1_1_logic.html#af29bdeeff97ca1a9a86fb160064f1a04", null ],
    [ "FactoryUpdate", "class_galactic_factories_1_1_logic_1_1_logic.html#a8969e806cbe7465ed455540e57629970", null ],
    [ "ModelCreate", "class_galactic_factories_1_1_logic_1_1_logic.html#a8bea5c741d811b8457819f03d8f93994", null ],
    [ "ModelDelete", "class_galactic_factories_1_1_logic_1_1_logic.html#a098abe3e38e558fa875b006a270ef4a4", null ],
    [ "ModelNameWithMostExpensiveUpgrade", "class_galactic_factories_1_1_logic_1_1_logic.html#aa5bec4ed4a26f91b51192c2f500f35a3", null ],
    [ "ModelsGetAll", "class_galactic_factories_1_1_logic_1_1_logic.html#aa840d64c450a5ef7d6d44e2cc4cdba90", null ],
    [ "ModelUpdate", "class_galactic_factories_1_1_logic_1_1_logic.html#a51f51b01e9a9b4fbd88a49f7e2a5aa12", null ],
    [ "MUSCreate", "class_galactic_factories_1_1_logic_1_1_logic.html#ac677e3d309b0060cb20cdf8f8049eb19", null ],
    [ "MUSDelete", "class_galactic_factories_1_1_logic_1_1_logic.html#ac74bd2363145481ed724fc3bb32fb8a3", null ],
    [ "MUSGetAll", "class_galactic_factories_1_1_logic_1_1_logic.html#af04b3adfed6fd980549cdfd7e557cff7", null ],
    [ "MUSUpdate", "class_galactic_factories_1_1_logic_1_1_logic.html#af60b5b83cded1f91c1f3800c14db8191", null ],
    [ "NumberofCrewRequireModel", "class_galactic_factories_1_1_logic_1_1_logic.html#abdda4af7362c4ffa2b5b4ca1b12ae91c", null ],
    [ "UpgradeCreate", "class_galactic_factories_1_1_logic_1_1_logic.html#a02b1192499f115b5b7973fab82fd1e22", null ],
    [ "UpgradeDelete", "class_galactic_factories_1_1_logic_1_1_logic.html#a9fab49dad10f6c5df9f052d6ae1a6b35", null ],
    [ "UpgradesGetAll", "class_galactic_factories_1_1_logic_1_1_logic.html#a4eaf86dd769c67a80ee1b31c7d51592d", null ],
    [ "UpgradeUpdate", "class_galactic_factories_1_1_logic_1_1_logic.html#adcc450a69617525c1ddc26be5be531ea", null ]
];