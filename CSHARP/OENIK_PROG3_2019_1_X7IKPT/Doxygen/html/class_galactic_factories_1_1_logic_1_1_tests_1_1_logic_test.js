var class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test =
[
    [ "AveragePriceOfFactoryTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#ac4bc7b3203909088c9ddeae900c2e218", null ],
    [ "FactoriesGetAll_ReturnsCorrectData", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a3e1b28f1344130506c6a6ec20118810b", null ],
    [ "FactoryCreateTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a6875d66945cbfe11912c7c983d888f36", null ],
    [ "FactoryDeleteTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a44632019ace7f5141c1347d035abc971", null ],
    [ "FactoryUpdateTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a9f29a2718449435dbeada9017dd06e8d", null ],
    [ "ModelCreateTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a1e418d245c92f347fd84fe04bae825cb", null ],
    [ "ModelDeleteTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#aeafdc9281c4d62d659896abe50e6ea89", null ],
    [ "ModelsGetAll_ReturnsCorrectData", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a0d1d5a0a90cb5ff286c1d767e6c1d230", null ],
    [ "ModelUpdateTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a13c406a1a26a04b2a69c06f6eb6f2f7f", null ],
    [ "ModelUpgradeSwitchDeleteTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#aba0bec2740d6abd4058bbbac1278cb55", null ],
    [ "ModelUpgradeSwitchGetAll_ReturnsCorrectData", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a4fa8ea651719c4e30118deb2eae37869", null ],
    [ "ModelUpgradeSwitchTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a9c85f42ccca46c0ac69a252fe38fc826", null ],
    [ "ModelUpgradeSwitchUpdateTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#aab1e03107dec735f8ef798e5afdf82bc", null ],
    [ "RequireCrewTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#aff152d453a702299282ed42e9cb55849", null ],
    [ "Setup", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a6d08a7e7e18b81924548e8eb1e7aca80", null ],
    [ "UpgradeCreateTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#afee49cb8a16abdb896fa67ffb9248da9", null ],
    [ "UpgradeDeleteTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#ac27d37a9f98ec7bae1ea22e796fa338a", null ],
    [ "UpgradesGetAll_ReturnsCorrectData", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a8f817b00fc7e614ed250f275a0ca3cbb", null ],
    [ "UpgradeUpdateTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html#a1d1d6865ac465f3202d0e322cbeaa52a", null ]
];