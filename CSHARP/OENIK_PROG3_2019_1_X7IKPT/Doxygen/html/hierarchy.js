var hierarchy =
[
    [ "DbContext", null, [
      [ "GalacticFactories.Data.GalacticFactoriesEntities", "class_galactic_factories_1_1_data_1_1_galactic_factories_entities.html", null ]
    ] ],
    [ "GalacticFactories.Data.Factories", "class_galactic_factories_1_1_data_1_1_factories.html", null ],
    [ "IDisposable", null, [
      [ "GalacticFactories.Repository.Repository", "class_galactic_factories_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "GalacticFactories.Logic.ILogic", "interface_galactic_factories_1_1_logic_1_1_i_logic.html", [
      [ "GalacticFactories.Logic.Logic", "class_galactic_factories_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "GalacticFactories.Repository.IRepository", "interface_galactic_factories_1_1_repository_1_1_i_repository.html", [
      [ "GalacticFactories.Repository.Repository", "class_galactic_factories_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "GalacticFactories.Logic.Tests.LogicTest", "class_galactic_factories_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "GalacticFactories.Data.Models", "class_galactic_factories_1_1_data_1_1_models.html", null ],
    [ "GalacticFactories.Data.ModelUpgradeSwitch", "class_galactic_factories_1_1_data_1_1_model_upgrade_switch.html", null ],
    [ "GalacticFactories.Program.Program", "class_galactic_factories_1_1_program_1_1_program.html", null ],
    [ "GalacticFactories.Data.Upgrades", "class_galactic_factories_1_1_data_1_1_upgrades.html", null ]
];