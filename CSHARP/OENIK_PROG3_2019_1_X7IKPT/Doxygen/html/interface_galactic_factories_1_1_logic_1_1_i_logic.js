var interface_galactic_factories_1_1_logic_1_1_i_logic =
[
    [ "AveragePriceofFactory", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a9211cc0d6d3402de6ccfb7932b3a9e02", null ],
    [ "FactoriesGetAll", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a62d935316d8b7255200cf863065029b0", null ],
    [ "FactoryCreate", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a55a6d0699c59e91af46620ff2a109950", null ],
    [ "FactoryDelete", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a0cc91584498bdb30955494dc0f746ed3", null ],
    [ "FactoryUpdate", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#af4a41d1ffda904765d50f6295291483f", null ],
    [ "ModelCreate", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#accac53ae61cbbe5b0e673654cd8d0a66", null ],
    [ "ModelDelete", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#ac06e230c007cedd3ead1a1d0ec79a094", null ],
    [ "ModelNameWithMostExpensiveUpgrade", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#afa3d0c1e8983bb72d8cbe85d764dc752", null ],
    [ "ModelsGetAll", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a0ac56da83a321b30e595b00ec255678f", null ],
    [ "ModelUpdate", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a522d7b88a6acef82c36f5249deba9731", null ],
    [ "MUSCreate", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a2422a4630e830c935248e66ecf308984", null ],
    [ "MUSDelete", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#af48fe5c8c915da4ce6965216f5ab9af0", null ],
    [ "MUSGetAll", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a37d53f7b657b12f11a8b0d98cce70fcd", null ],
    [ "MUSUpdate", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#ac44e905a28b141efb5d9f78385929f93", null ],
    [ "NumberofCrewRequireModel", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#aac49a780de9a1430a68b902d62ff86fe", null ],
    [ "UpgradeCreate", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a25bb7a5df7b32d6b22fb80539a2a67b1", null ],
    [ "UpgradeDelete", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a89cc4d03724b160284afdb9ab38da593", null ],
    [ "UpgradesGetAll", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a65ecfe8b67602363dc8d81dd1d567ebf", null ],
    [ "UpgradeUpdate", "interface_galactic_factories_1_1_logic_1_1_i_logic.html#a4f4df046dc80f35ae9f5522a327295a5", null ]
];