var interface_galactic_factories_1_1_repository_1_1_i_repository =
[
    [ "FactoriesGetAll", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#add384cc2c6eb70cded9c1500ae13218c", null ],
    [ "FactoryCreate", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a9bc128cac5d458881ecdc6fc9c98af20", null ],
    [ "FactoryDelete", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a8f6ac1e3ef21be6c07c5bf752caeb290", null ],
    [ "FactoryUpdate", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#afc73a7139d35f94b3990af4b6d8c4cd3", null ],
    [ "ModelCreate", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a7d1aa25a1bdd329a1e1dbafc47a93077", null ],
    [ "ModelDelete", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#aeaa25c37da8f045bcd5f898142b335bf", null ],
    [ "ModelsGetAll", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a7e1aedfaf361557a2c3007c7c343717c", null ],
    [ "ModelUpdate", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a051d7e15de040f6c9a0f38677327afc6", null ],
    [ "MUSCreate", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#ad8d48639d2ac849e0eb0b9bfdc02a284", null ],
    [ "MUSDelete", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a6185d29fade45ee1f4b4290c539dd69c", null ],
    [ "MUSGetAll", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a16597af4058ae076a97381e39fe2b57c", null ],
    [ "MUSUpdate", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#aec4d130546d225d8b79c5d05b5f90f1f", null ],
    [ "UpgradeCreate", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a07089c38e9eaa055fbe16d72edf59a39", null ],
    [ "UpgradeDelete", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a6dc90ff4dda66faef8ad2ecbf76a0888", null ],
    [ "UpgradesGetAll", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a67094131f60d18dba9e4ed9eab30c7be", null ],
    [ "UpgradeUpdate", "interface_galactic_factories_1_1_repository_1_1_i_repository.html#a0d4d2a591559438e297162b39f43adec", null ]
];