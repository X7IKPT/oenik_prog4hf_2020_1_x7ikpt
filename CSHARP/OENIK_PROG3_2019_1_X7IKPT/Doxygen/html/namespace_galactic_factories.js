var namespace_galactic_factories =
[
    [ "Data", "namespace_galactic_factories_1_1_data.html", "namespace_galactic_factories_1_1_data" ],
    [ "Logic", "namespace_galactic_factories_1_1_logic.html", "namespace_galactic_factories_1_1_logic" ],
    [ "Program", "namespace_galactic_factories_1_1_program.html", "namespace_galactic_factories_1_1_program" ],
    [ "Repository", "namespace_galactic_factories_1_1_repository.html", "namespace_galactic_factories_1_1_repository" ]
];