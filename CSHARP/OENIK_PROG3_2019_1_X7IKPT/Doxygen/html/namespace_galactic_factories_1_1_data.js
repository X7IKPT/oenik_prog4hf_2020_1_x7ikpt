var namespace_galactic_factories_1_1_data =
[
    [ "Factories", "class_galactic_factories_1_1_data_1_1_factories.html", "class_galactic_factories_1_1_data_1_1_factories" ],
    [ "GalacticFactoriesEntities", "class_galactic_factories_1_1_data_1_1_galactic_factories_entities.html", "class_galactic_factories_1_1_data_1_1_galactic_factories_entities" ],
    [ "Models", "class_galactic_factories_1_1_data_1_1_models.html", "class_galactic_factories_1_1_data_1_1_models" ],
    [ "ModelUpgradeSwitch", "class_galactic_factories_1_1_data_1_1_model_upgrade_switch.html", "class_galactic_factories_1_1_data_1_1_model_upgrade_switch" ],
    [ "Upgrades", "class_galactic_factories_1_1_data_1_1_upgrades.html", "class_galactic_factories_1_1_data_1_1_upgrades" ]
];