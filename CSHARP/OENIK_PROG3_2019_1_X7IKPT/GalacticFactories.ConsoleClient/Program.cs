﻿using GalacticFactories.Data;
using GalacticFactories.Logic;
using GalacticFactories.Repository;
using GalacticFactories.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GalacticFactories.ConsoleClient
{
    public class Factories
    {
        public int Id { get; set; }
        public string FactoryName { get; set; }
        public string Planet { get; set; }
        public string Leader { get; set; }
        public string Side { get; set; }
        public int StrategicValue { get; set; }

        public override string ToString()
        {
            return $"Id = {Id} \tFacoryName = {FactoryName}\t" +
                $"Planet= {Planet}\t" +
                $"Leader= {Leader}\t" +
                $"Side= {Side}\t" +
                $"StrategicValue= {StrategicValue}\t";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting...");
            Console.ReadLine();

            string url = "http://localhost:53327/api/FactoriesApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Factories>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }

                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Factories.FactoryName), "TestFactory");
                postData.Add(nameof(Factories.Planet), "TestPlanet");
                postData.Add(nameof(Factories.Leader), "TestLeader");
                postData.Add(nameof(Factories.Side), "TestSide");
                postData.Add(nameof(Factories.StrategicValue), "12");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Add: " + response);
                Console.WriteLine("All: " + json);
                Console.ReadLine();

                int factoryId = JsonConvert.DeserializeObject<List<Factories>>(json).Single(x => x.FactoryName == "TestFactory").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Factories.Id), factoryId.ToString());
                postData.Add(nameof(Factories.FactoryName), "TestFactory");
                postData.Add(nameof(Factories.Planet), "TestPlanet");
                postData.Add(nameof(Factories.Leader), "TestLeader");
                postData.Add(nameof(Factories.Side), "TestSide2");
                postData.Add(nameof(Factories.StrategicValue), "13");

                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Mod: " + response);
                Console.WriteLine("All: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + factoryId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Del: " + response);
                Console.WriteLine("All: " + json);
                Console.ReadLine();
            }
        }
    }
}
