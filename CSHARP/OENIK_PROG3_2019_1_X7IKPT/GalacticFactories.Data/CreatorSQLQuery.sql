﻿create table Factories(
ID numeric(5) not null,
FactoryName varchar(50),
Planet varchar(50),
Leader varchar(50),
Side VarChar(50),
StrategicValue numeric(5),
constraint Factories_pk primary key (ID));

insert into Factories values (1,'Weapons Factory Alpha','Cymoon 1','Overseer Aggadeen','Galactic Empire',8);
insert into Factories values (2,'Geonosian droid factories','Geonosis','Archduke Poggle','Galactic Empire',6);
insert into Factories values (3,'Incom Corporation','Fresia','Unknown','Republic',9);

Create table Models(
ID numeric(5) not null,
FactoryID numeric(5) not null,
ModelName varchar(50),
RequiresCrew bit,
FirePower numeric(20),
Color varchar(50),
Price numeric(20),
constraint Models_pk primary key (ID),
constraint Models_fk foreign key(FactoryID) references Factories(ID));

insert into Models values(1,1,'Tie Fighter',1,1000,'Grey',210000);
insert into Models values(2,1,'74-Z speeder bikes',1,70,'Grey',3300);
insert into Models values(3,1,'AT-AT',1,90000,'Grey',40000000);
insert into Models values(4,1,'Imperial I-class Star Destroyer',1,3400000,'Grey',2000000000);
insert into Models values(5,2,'B1 battle droid',0,15,'Red',14);
insert into Models values(6,2,'B2 super battle droid',0,30,'Grey',25);
insert into Models values(7,2,'Droidekas',0,40,'Black',30);
insert into Models values(8,3,'T-65B X-wing starfighter',1,1400,'Red',300000);
insert into Models values(9,3,'UT-60D U-wing starfighter',1,1350,'Blue',270000);
insert into Models values(10,3,'T-47 airspeeder',1,900,'White',10000);
insert into Models values(11,3,'Mon Calamari Star Cruiser',1,3000000,'White',1900000000);

Create table Upgrades(
ID numeric(5) not null,
UpgradeType varchar(50),
UpgradeName varchar(50),
FirePowerIncrease numeric(20),
Reuseable bit,
Price numeric (20)
constraint Upgrades_pk primary key(ID));

insert into Upgrades values(1,'Weapon','LX-4 proton mines',1000,0,6);
insert into Upgrades values(2,'Weapon','E-11 blaster rifle',20,1,12);
insert into Upgrades values(3,'Weapon','KY9 laser cannons',350,1,2000);
insert into Upgrades values(4,'Shield','Deflector shield',0,1,3000);
insert into Upgrades values(5,'Engine','Gemon-4 ion engines',0,1,400000);
insert into Upgrades values(6,'Engine','Sublight thrusters',0,1,380000);
insert into Upgrades values(7,'Weapon','MG7 proton torpedo',200,0,1800);

Create table ModelUpgradeSwitch(
ID numeric(5) not null,
UpgradeID numeric(5) not null,
ModelID numeric(5) not null,
constraint ModelUpgradeSwitch_pk primary key (ID),
constraint ModelSwitch_fk foreign key (ModelID) references Models(ID),
constraint UpgradeSwitch_fk foreign key (UpgradeID) references Upgrades(ID));

insert into ModelUpgradeSwitch values(1,1,2);
insert into ModelUpgradeSwitch values(2,1,3);
insert into ModelUpgradeSwitch values(3,1,1);
insert into ModelUpgradeSwitch values(4,2,5);
insert into ModelUpgradeSwitch values(5,2,6);
insert into ModelUpgradeSwitch values(6,2,7);
insert into ModelUpgradeSwitch values(7,3,8);
insert into ModelUpgradeSwitch values(8,3,1);
insert into ModelUpgradeSwitch values(9,4,1);
insert into ModelUpgradeSwitch values(10,4,8);
insert into ModelUpgradeSwitch values(11,4,9);
insert into ModelUpgradeSwitch values(12,5,4);
insert into ModelUpgradeSwitch values(13,5,11);
insert into ModelUpgradeSwitch values(14,6,4);
insert into ModelUpgradeSwitch values(15,6,11);
insert into ModelUpgradeSwitch values(16,7,1);
insert into ModelUpgradeSwitch values(17,7,11);
insert into ModelUpgradeSwitch values(18,7,4);
insert into ModelUpgradeSwitch values(19,7,9);
insert into ModelUpgradeSwitch values(20,7,8);

select * from Factories;
select * from Models;
select * from Upgrades;
select * from ModelUpgradeSwitch;

drop table ModelUpgradeSwitch;
drop table Upgrades;
drop table Models;
drop table Factories;