//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GalacticFactories.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Models
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Models()
        {
            this.ModelUpgradeSwitch = new HashSet<ModelUpgradeSwitch>();
        }
    
        public decimal ID { get; set; }
        public decimal FactoryID { get; set; }
        public string ModelName { get; set; }
        public Nullable<bool> RequiresCrew { get; set; }
        public Nullable<decimal> FirePower { get; set; }
        public string Color { get; set; }
        public Nullable<decimal> Price { get; set; }
    
        public virtual Factories Factories { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModelUpgradeSwitch> ModelUpgradeSwitch { get; set; }
    }
}
