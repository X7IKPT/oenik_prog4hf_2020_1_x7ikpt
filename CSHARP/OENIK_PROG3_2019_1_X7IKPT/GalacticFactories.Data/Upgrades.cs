//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GalacticFactories.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Upgrades
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Upgrades()
        {
            this.ModelUpgradeSwitch = new HashSet<ModelUpgradeSwitch>();
        }
    
        public decimal ID { get; set; }
        public string UpgradeType { get; set; }
        public string UpgradeName { get; set; }
        public Nullable<decimal> FirePowerIncrease { get; set; }
        public Nullable<bool> Reuseable { get; set; }
        public Nullable<decimal> Price { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModelUpgradeSwitch> ModelUpgradeSwitch { get; set; }
    }
}
