﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GalacticFactories.Logic.Tests
{
    using System.Linq;
    using GalacticFactories.Data;
    using GalacticFactories.Logic;
    using GalacticFactories.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Logic test class
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IRepository> factoryRepMoq;
        private Mock<IRepository> modelsRepMoq;
        private Mock<IRepository> upgradesRepMoq;
        private Mock<IRepository> musRepMoq;

        /// <summary>
        /// Test setup method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.factoryRepMoq = new Mock<IRepository>();
            Factories f1 = new Factories() { ID = 1, FactoryName = "Testname1", Planet = "Testplanet1", Leader = "Testleader1", Side = "Testside1", StrategicValue = 1 };
            Factories f2 = new Factories() { ID = 2, FactoryName = "Testname2", Planet = "Testplanet2", Leader = "Testleader2", Side = "Testside2", StrategicValue = 2 };

            this.factoryRepMoq.Setup(m => m.FactoriesGetAll()).Returns(new[] { f1, f2 });

            this.modelsRepMoq = new Mock<IRepository>();
            Models m1 = new Models() { ID = 1, FactoryID = 1, ModelName = "Testmodel1", RequiresCrew = true, FirePower = 1, Color = "Testcolor1", Price = 1 };
            Models m2 = new Models() { ID = 2, FactoryID = 1, ModelName = "Testmodel2", RequiresCrew = false, FirePower = 2, Color = "Testcolor2", Price = 3 };
            Models m3 = new Models() { ID = 3, FactoryID = 2, ModelName = "Testmodel3", RequiresCrew = true, FirePower = 3, Color = "Testcolor3", Price = 1 };
            Models m4 = new Models() { ID = 4, FactoryID = 2, ModelName = "Testmodel4", RequiresCrew = false, FirePower = 4, Color = "Testcolor4", Price = 3 };

            this.modelsRepMoq.Setup(m => m.ModelsGetAll()).Returns(new[] { m1, m2, m3, m4 });

            this.upgradesRepMoq = new Mock<IRepository>();
            Upgrades u1 = new Upgrades() { ID = 1, UpgradeType = "Testtype1", UpgradeName = "Testupgrade1", FirePowerIncrease = 1, Reuseable = true, Price = 1 };
            Upgrades u2 = new Upgrades() { ID = 2, UpgradeType = "Testtype2", UpgradeName = "Testupgrade2", FirePowerIncrease = 2, Reuseable = false, Price = 2 };
            Upgrades u3 = new Upgrades() { ID = 3, UpgradeType = "Testtype3", UpgradeName = "Testupgrade3", FirePowerIncrease = 3, Reuseable = true, Price = 3 };
            Upgrades u4 = new Upgrades() { ID = 4, UpgradeType = "Testtype4", UpgradeName = "Testupgrade4", FirePowerIncrease = 4, Reuseable = false, Price = 4 };

            this.upgradesRepMoq.Setup(m => m.UpgradesGetAll()).Returns(new[] { u1, u2, u3, u4 });

            this.musRepMoq = new Mock<IRepository>();
            ModelUpgradeSwitch mus1 = new ModelUpgradeSwitch() { ID = 1, UpgradeID = 1, ModelID = 1 };
            ModelUpgradeSwitch mus2 = new ModelUpgradeSwitch() { ID = 2, UpgradeID = 2, ModelID = 2 };
            ModelUpgradeSwitch mus3 = new ModelUpgradeSwitch() { ID = 3, UpgradeID = 1, ModelID = 3 };
            ModelUpgradeSwitch mus4 = new ModelUpgradeSwitch() { ID = 4, UpgradeID = 4, ModelID = 1 };
            ModelUpgradeSwitch mus5 = new ModelUpgradeSwitch() { ID = 5, UpgradeID = 1, ModelID = 4 };

            this.musRepMoq.Setup(m => m.MUSGetAll()).Returns(new[] { mus1, mus2, mus3, mus4, mus5 });
        }

        /// <summary>
        /// Get All factories test method
        /// </summary>
        [Test]
        public void FactoriesGetAll_ReturnsCorrectData()
        {
            Logic logic = new Logic(this.factoryRepMoq.Object);

            var res = logic.FactoriesGetAll();

            Assert.That(res.Count(), Is.EqualTo(2));
        }

        /// <summary>
        /// Get all models test method
        /// </summary>
        [Test]
        public void ModelsGetAll_ReturnsCorrectData()
        {
            Logic logic = new Logic(this.modelsRepMoq.Object);

            var res = logic.ModelsGetAll();

            Assert.That(res.Count(), Is.EqualTo(4));
        }

        /// <summary>
        /// Get all upgrades test method
        /// </summary>
        [Test]
        public void UpgradesGetAll_ReturnsCorrectData()
        {
            Logic logic = new Logic(this.upgradesRepMoq.Object);

            var res = logic.UpgradesGetAll();

            Assert.That(res.Count(), Is.EqualTo(4));
        }

        /// <summary>
        /// Get all MUS test method
        /// </summary>
        [Test]
        public void ModelUpgradeSwitchGetAll_ReturnsCorrectData()
        {
            Logic logic = new Logic(this.musRepMoq.Object);

            var res = logic.MUSGetAll();

            Assert.That(res.Count(), Is.EqualTo(5));
        }

        /// <summary>
        /// Create new factory test method
        /// </summary>
        [Test]
        public void FactoryCreateTest()
        {
            Logic logic = new Logic(this.factoryRepMoq.Object);

            logic.FactoryCreate("testname", "testplanet", "testleader", "testside", 3);

            this.factoryRepMoq.Verify(x => x.FactoryCreate(It.IsAny<Factories>()), Times.Once);
        }

        /// <summary>
        /// Create new model test method
        /// </summary>
        [Test]
        public void ModelCreateTest()
        {
            Logic logic = new Logic(this.modelsRepMoq.Object);

            logic.ModelCreate(1, "testname", true, 10, "testcolor", 666);

            this.modelsRepMoq.Verify(x => x.ModelCreate(It.IsAny<Models>()), Times.Once);
        }

        /// <summary>
        /// Create new upgrade test method
        /// </summary>
        [Test]
        public void UpgradeCreateTest()
        {
            Logic logic = new Logic(this.upgradesRepMoq.Object);

            logic.UpgradeCreate("testtype", "testname", 666, true, 666);

            this.upgradesRepMoq.Verify(x => x.UpgradeCreate(It.IsAny<Upgrades>()), Times.Once);
        }

        /// <summary>
        /// Create new MUS test method
        /// </summary>
        [Test]
        public void ModelUpgradeSwitchTest()
        {
            Logic logic = new Logic(this.musRepMoq.Object);

            logic.MUSCreate(1, 1);

            this.musRepMoq.Verify(x => x.MUSCreate(It.IsAny<ModelUpgradeSwitch>()), Times.Once);
        }

        /// <summary>
        /// Delete factory test method
        /// </summary>
        [Test]
        public void FactoryDeleteTest()
        {
            Logic logic = new Logic(this.factoryRepMoq.Object);

            logic.FactoryDelete(1);

            this.factoryRepMoq.Verify(x => x.FactoryDelete(1), Times.Once);
        }

        /// <summary>
        /// Delete test method
        /// </summary>
        [Test]
        public void ModelDeleteTest()
        {
            Logic logic = new Logic(this.modelsRepMoq.Object);

            logic.ModelDelete(1);

            this.modelsRepMoq.Verify(x => x.ModelDelete(1), Times.Once);
        }

        /// <summary>
        /// Delete upgrade test method
        /// </summary>
        [Test]
        public void UpgradeDeleteTest()
        {
            Logic logic = new Logic(this.upgradesRepMoq.Object);

            logic.UpgradeDelete(1);

            this.upgradesRepMoq.Verify(x => x.UpgradeDelete(1), Times.Once);
        }

        /// <summary>
        /// Delete MUS test method
        /// </summary>
        [Test]
        public void ModelUpgradeSwitchDeleteTest()
        {
            Logic logic = new Logic(this.musRepMoq.Object);

            logic.MUSDelete(1);

            this.musRepMoq.Verify(x => x.MUSDelete(1), Times.Once);
        }

        /// <summary>
        /// Update factory test method
        /// </summary>
        [Test]
        public void FactoryUpdateTest()
        {
            Logic logic = new Logic(this.factoryRepMoq.Object);

            logic.FactoryUpdate(1, "planet", "testchangeplanet");

            this.factoryRepMoq.Verify(x => x.FactoryUpdate(1, "planet", "testchangeplanet"), Times.Once);
        }

        /// <summary>
        /// Update mode test method
        /// </summary>
        [Test]
        public void ModelUpdateTest()
        {
            Logic logic = new Logic(this.modelsRepMoq.Object);

            logic.ModelUpdate(1, "color", "testupdatecolor");

            this.modelsRepMoq.Verify(x => x.ModelUpdate(1, "color", "testupdatecolor"), Times.Once);
        }

        /// <summary>
        /// Update upgrade test method
        /// </summary>
        [Test]
        public void UpgradeUpdateTest()
        {
            Logic logic = new Logic(this.upgradesRepMoq.Object);

            logic.UpgradeUpdate(1, "upgradetype", "TestType");

            this.upgradesRepMoq.Verify(x => x.UpgradeUpdate(1, "upgradetype", "TestType"), Times.Once);
        }

        /// <summary>
        /// Update MUS test method
        /// </summary>
        [Test]
        public void ModelUpgradeSwitchUpdateTest()
        {
            Logic logic = new Logic(this.musRepMoq.Object);

            logic.MUSUpdate(1, "modelid", "2");

            this.musRepMoq.Verify(x => x.MUSUpdate(1, "modelid", "2"), Times.Once);
        }

        /// <summary>
        /// Average factory prices test method
        /// </summary>
        [Test]
        public void AveragePriceOfFactoryTest()
        {
            Logic logic = new Logic(this.modelsRepMoq.Object);

            int res = logic.AveragePriceofFactory(1);

            Assert.That(res, Is.EqualTo(2));
        }

        /// <summary>
        /// RequiresCrew test method
        /// </summary>
        [Test]
        public void RequireCrewTest()
        {
            Logic logic = new Logic(this.modelsRepMoq.Object);

            int res = logic.NumberofCrewRequireModel();

            Assert.That(res, Is.EqualTo(2));
        }
    }
}
