﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GalacticFactories.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalacticFactories.Data;
    using GalacticFactories.Repository;

    /// <summary>
    /// Logic interface
    /// </summary>
    public interface ILogic
    {
        bool ChangeFactory(int id, string Name, string Planet, string Leader, string Side, int StratValue);
        Factories GetOneFactory(int id);
        /// <summary>
        /// Return factories
        /// </summary>
        /// <returns>factories</returns>
        IEnumerable<Factories> FactoriesGetAll();

        /// <summary>
        /// Create factories
        /// </summary>
        /// <param name="factoryname">New factory name</param>
        /// <param name="planet">new planet name</param>
        /// <param name="leader">new leader name</param>
        /// <param name="side">new side</param>
        /// <param name="strategicvalue">new strategic value</param>
        void FactoryCreate(string factoryname, string planet, string leader, string side, int strategicvalue);

        /// <summary>
        /// Factory update method
        /// </summary>
        /// <param name="id">factory id</param>
        /// <param name="update">factory field</param>
        /// <param name="change">new data</param>
        void FactoryUpdate(int id, string update, string change);

        /// <summary>
        /// Factory delete method
        /// </summary>
        /// <param name="id">factory id</param>
        void FactoryDelete(int id);
        bool DeleteFactory(int id);

        /// <summary>
        /// Models get method
        /// </summary>
        /// <returns>Models</returns>
        IEnumerable<Models> ModelsGetAll();

        /// <summary>
        /// Create new model
        /// </summary>
        /// <param name="factoryid">factory id</param>
        /// <param name="modelname">new name</param>
        /// <param name="requirescrew">new requirescrew</param>
        /// <param name="firepower">new firepower</param>
        /// <param name="color">new color</param>
        /// <param name="price">new price</param>
        void ModelCreate(int factoryid, string modelname, bool requirescrew, int firepower, string color, int price);

        /// <summary>
        /// Model update method
        /// </summary>
        /// <param name="id">model id</param>
        /// <param name="update">model field</param>
        /// <param name="change">new data</param>
        void ModelUpdate(int id, string update, string change);

        /// <summary>
        /// model delete method
        /// </summary>
        /// <param name="id">model id</param>
        void ModelDelete(int id);

        /// <summary>
        /// Upgrades get method
        /// </summary>
        /// <returns>Upgrades</returns>
        IEnumerable<Upgrades> UpgradesGetAll();

        /// <summary>
        /// Create new upgrade
        /// </summary>
        /// <param name="upgradetype">new type</param>
        /// <param name="upgradename">new name</param>
        /// <param name="firepowerincrease">new firepower increase</param>
        /// <param name="reuseable">new reuseable</param>
        /// <param name="price">new price</param>
        void UpgradeCreate(string upgradetype, string upgradename, int firepowerincrease, bool reuseable, int price);

        /// <summary>
        /// Upgrade update method
        /// </summary>
        /// <param name="id">upgrade id</param>
        /// <param name="update">upgrade field</param>
        /// <param name="change">new data</param>
        void UpgradeUpdate(int id, string update, string change);

        /// <summary>
        /// Upgrade delete method
        /// </summary>
        /// <param name="id">upgrade id</param>
        void UpgradeDelete(int id);

        /// <summary>
        /// Model upgrade switch get method
        /// </summary>
        /// <returns>model upgrade switch</returns>
        IEnumerable<ModelUpgradeSwitch> MUSGetAll();

        /// <summary>
        /// Create new mus
        /// </summary>
        /// <param name="upgradeid">upgrade id</param>
        /// <param name="modelid">model id</param>
        void MUSCreate(int upgradeid, int modelid);

        /// <summary>
        /// Mus update method
        /// </summary>
        /// <param name="id">mus id</param>
        /// <param name="update">mus field</param>
        /// <param name="change">new data</param>
        void MUSUpdate(int id, string update, string change);

        /// <summary>
        /// Mus delete method
        /// </summary>
        /// <param name="id">mus id</param>
        void MUSDelete(int id);

        /// <summary>
        /// Average price of the chosen factory
        /// </summary>
        /// <param name="id">factory id</param>
        /// <returns>average price</returns>
        int AveragePriceofFactory(int id);

        /// <summary>
        /// Most expensive upgrade model name
        /// </summary>
        /// <returns>model name</returns>
        string ModelNameWithMostExpensiveUpgrade();

        /// <summary>
        /// Number of requires crew models
        /// </summary>
        /// <returns>count of requires crew models</returns>
        int NumberofCrewRequireModel();
    }
}
