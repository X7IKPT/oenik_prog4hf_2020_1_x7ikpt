﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GalacticFactories.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalacticFactories.Data;
    using GalacticFactories.Repository;

    /// <summary>
    /// Logic class
    /// </summary>
    public class Logic : ILogic
    {
        private readonly IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Logic class with repository
        /// </summary>
        /// <param name="repository">Database repository</param>
        public Logic(IRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Return factories
        /// </summary>
        /// <returns>factories</returns>
        public IEnumerable<Factories> FactoriesGetAll()
        {
            return this.repository.FactoriesGetAll();
        }

        public Factories GetOneFactory(int id)
        {
            return repository.GetOneFactory(id);
        }

        public bool ChangeFactory(int id, string Name, string Planet, string Leader, string Side, int StratValue)
        {
            var factory = repository.FactoriesGetAll().SingleOrDefault(x => x.ID == id);
            return repository.ChangeFactory(id, factory, Name, Planet, Leader, Side, StratValue);
        }

        /// <summary>
        /// Create factories
        /// </summary>
        /// <param name="factoryname">New factory name</param>
        /// <param name="planet">new planet name</param>
        /// <param name="leader">new leader name</param>
        /// <param name="side">new side</param>
        /// <param name="strategicvalue">new strategic value</param>
        public void FactoryCreate(string factoryname, string planet, string leader, string side, int strategicvalue)
        {
            int maxid = (int)this.repository.FactoriesGetAll().Select(x => x.ID).Max();

            Factories f = new Factories()
            {
                ID = maxid + 1,
                FactoryName = factoryname,
                Planet = planet,
                Leader = leader,
                Side = side,
                StrategicValue = strategicvalue
            };

            this.repository.FactoryCreate(f);
        }

        /// <summary>
        /// Factory update method
        /// </summary>
        /// <param name="id">factory id</param>
        /// <param name="update">factory field</param>
        /// <param name="change">new data</param>
        public void FactoryUpdate(int id, string update, string change)
        {
            this.repository.FactoryUpdate(id, update, change);
        }

        /// <summary>
        /// Factory delete method
        /// </summary>
        /// <param name="id">factory id</param>
        public void FactoryDelete(int id)
        {
            this.repository.FactoryDelete(id);
        }

        public bool DeleteFactory(int id)
        {
            this.repository.FactoryDelete(id);
            return true;
        }

        /// <summary>
        /// Models get method
        /// </summary>
        /// <returns>Models</returns>
        public IEnumerable<Models> ModelsGetAll()
        {
            return this.repository.ModelsGetAll();
        }

        /// <summary>
        /// Create new model
        /// </summary>
        /// <param name="factoryid">factory id</param>
        /// <param name="modelname">new name</param>
        /// <param name="requirescrew">new requirescrew</param>
        /// <param name="firepower">new firepower</param>
        /// <param name="color">new color</param>
        /// <param name="price">new price</param>
        public void ModelCreate(int factoryid, string modelname, bool requirescrew, int firepower, string color, int price)
        {
            int maxid = (int)this.repository.ModelsGetAll().Select(x => x.ID).Max();

            Models m = new Models()
            {
                ID = maxid + 1,
                FactoryID = factoryid,
                ModelName = modelname,
                RequiresCrew = requirescrew,
                FirePower = firepower,
                Color = color,
                Price = price
            };

            this.repository.ModelCreate(m);
        }

        /// <summary>
        /// Model update method
        /// </summary>
        /// <param name="id">model id</param>
        /// <param name="update">model field</param>
        /// <param name="change">new data</param>
        public void ModelUpdate(int id, string update, string change)
        {
            this.repository.ModelUpdate(id, update, change);
        }

        /// <summary>
        /// model delete method
        /// </summary>
        /// <param name="id">model id</param>
        public void ModelDelete(int id)
        {
            this.repository.ModelDelete(id);
        }

        /// <summary>
        /// Upgrades get method
        /// </summary>
        /// <returns>Upgrades</returns>
        public IEnumerable<Upgrades> UpgradesGetAll()
        {
            return this.repository.UpgradesGetAll();
        }

        /// <summary>
        /// Create new upgrade
        /// </summary>
        /// <param name="upgradetype">new type</param>
        /// <param name="upgradename">new name</param>
        /// <param name="firepowerincrease">new firepower increase</param>
        /// <param name="reuseable">new reuseable</param>
        /// <param name="price">new price</param>
        public void UpgradeCreate(string upgradetype, string upgradename, int firepowerincrease, bool reuseable, int price)
        {
            int maxid = (int)this.repository.UpgradesGetAll().Select(x => x.ID).Max();

            Upgrades u = new Upgrades()
            {
                ID = maxid + 1,
                UpgradeType = upgradetype,
                UpgradeName = upgradename,
                FirePowerIncrease = firepowerincrease,
                Reuseable = reuseable,
                Price = price
            };

            this.repository.UpgradeCreate(u);
        }

        /// <summary>
        /// Upgrade update method
        /// </summary>
        /// <param name="id">upgrade id</param>
        /// <param name="update">upgrade field</param>
        /// <param name="change">new data</param>
        public void UpgradeUpdate(int id, string update, string change)
        {
            this.repository.UpgradeUpdate(id, update, change);
        }

        /// <summary>
        /// Upgrade delete method
        /// </summary>
        /// <param name="id">upgrade id</param>
        public void UpgradeDelete(int id)
        {
            this.repository.UpgradeDelete(id);
        }

        /// <summary>
        /// Model upgrade switch get method
        /// </summary>
        /// <returns>model upgrade switch</returns>
        public IEnumerable<ModelUpgradeSwitch> MUSGetAll()
        {
            return this.repository.MUSGetAll();
        }

        /// <summary>
        /// Create new mus
        /// </summary>
        /// <param name="upgradeid">upgrade id</param>
        /// <param name="modelid">model id</param>
        public void MUSCreate(int upgradeid, int modelid)
        {
            int maxid = (int)this.repository.MUSGetAll().Select(x => x.ID).Max();

            ModelUpgradeSwitch mus = new ModelUpgradeSwitch()
            {
                ID = maxid + 1,
                UpgradeID = upgradeid,
                ModelID = modelid
            };

            this.repository.MUSCreate(mus);
        }

        /// <summary>
        /// Mus update method
        /// </summary>
        /// <param name="id">mus id</param>
        /// <param name="update">mus field</param>
        /// <param name="change">new data</param>
        public void MUSUpdate(int id, string update, string change)
        {
            this.repository.MUSUpdate(id, update, change);
        }

        /// <summary>
        /// Mus delete method
        /// </summary>
        /// <param name="id">mus id</param>
        public void MUSDelete(int id)
        {
            this.repository.MUSDelete(id);
        }

        /// <summary>
        /// Average price of the chosen factory
        /// </summary>
        /// <param name="id">factory id</param>
        /// <returns>average price</returns>
        public int AveragePriceofFactory(int id)
        {
            var res = this.repository.ModelsGetAll().Where(x => x.FactoryID == id);

            return (int)res.Average(x => x.Price);
        }

        /// <summary>
        /// Number of requires crew models
        /// </summary>
        /// <returns>count of requires crew models</returns>
        public int NumberofCrewRequireModel()
        {
            return this.repository.ModelsGetAll().Where(x => x.RequiresCrew == true).Count();
        }

        /// <summary>
        /// Most expensive upgrade model name
        /// </summary>
        /// <returns>model name</returns>
        public string ModelNameWithMostExpensiveUpgrade()
        {
            var maxp = this.repository.UpgradesGetAll().Max(x => x.Price).Value;
            var maxid = this.repository.UpgradesGetAll().Where(x => x.Price == maxp).First();
            var musid = this.repository.MUSGetAll().Where(x => x.UpgradeID == maxid.ID).First();
            var modelid = this.repository.ModelsGetAll().Where(x => x.ID == musid.ModelID).First();
            return modelid.ModelName;
        }
    }
}
