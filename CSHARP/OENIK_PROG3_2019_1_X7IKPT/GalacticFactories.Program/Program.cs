﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GalacticFactories.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using GalacticFactories.Data;
    using GalacticFactories.Logic;
    using GalacticFactories.Repository;

    /// <summary>
    /// Program exe class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main method
        /// </summary>
        /// <param name="args">args parameter</param>
        public static void Main(string[] args)
        {
            MainMenu();

            Console.Clear();

            Console.WriteLine("May the Force be with you");

            Thread.Sleep(2000);
        }

        private static void MainMenu()
        {
            List<string> mainMenu = new List<string>() { "Listák", "Módosítás", "Törlés", "Beszúrás", "Ajánlatkérés" };
            List<string> dataBaseList = new List<string>() { "Gyárak", "Modellek", "Fejlesztések" };

            int menuNumber = 0;

            while (menuNumber != 9)
            {
                menuNumber = MenuNumberValidator(mainMenu);

                switch (menuNumber)
                {
                    case 1:
                        ListMenu(0);
                        break;
                    case 2:
                        ChangeMenu(dataBaseList);
                        break;
                    case 3:
                        DeleteMenu(dataBaseList);
                        break;
                    case 4:
                        InsertMenu(dataBaseList);
                        break;
                    case 5:
                        Bid();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void ListMenu(int x)
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            List<string> listMenu = new List<string>() { "Gyárak", "Modellek", " Fejlesztések", "Modell - Fejlesztés kapcsoló", "Egyedi listák" };

            Console.Clear();

            int menuNumber = 0;

            if (x == 0)
            {
                menuNumber = MenuNumberValidator(listMenu);
            }
            else
            {
                menuNumber = x;
            }

            switch (menuNumber)
            {
                case 1:
                    var factories = logic.FactoriesGetAll();
                    Console.WriteLine("ID" + "\t\t" + "Name" + "\t\t" + "Planet" + "\t\t" + "Leader" + "\t\t" + "Side" + "\t\t" + "Strategic Value");
                    foreach (var item in factories)
                    {
                        Console.WriteLine(item.ID + "\t\t" + item.FactoryName + "\t\t" + item.Planet + "\t\t" + item.Leader + "\t\t" + item.Side + "\t\t" + item.StrategicValue);
                    }

                    break;
                case 2:
                    var models = logic.ModelsGetAll();
                    Console.WriteLine("ID" + "\t\t" + "Name" + "\t\t" + "Crew" + "\t\t" + "FirePower" + "\t\t" + "Color" + "\t\t" + "Price");
                    foreach (var item in models)
                    {
                        Console.WriteLine(item.ID + "\t\t" + item.ModelName + "\t\t" + item.RequiresCrew + "\t\t" + item.FirePower + "\t\t" + item.Color + "\t\t" + item.Price);
                    }

                    break;
                case 3:
                    var upgrades = logic.UpgradesGetAll();
                    Console.WriteLine("ID" + "\t\t" + "Type" + "\t\t" + "Name" + "\t\t" + "Firepower Increase" + "\t\t" + "Reuseable" + "\t\t" + "Price");
                    foreach (var item in upgrades)
                    {
                        Console.WriteLine(item.ID + "\t\t" + item.UpgradeType + "\t\t" + item.UpgradeName + "\t\t" + item.FirePowerIncrease + "\t\t" + item.Reuseable + "\t\t" + item.Price);
                    }

                    break;
                case 4:
                    var mus = logic.MUSGetAll();
                    Console.WriteLine("ID" + "\t\t" + "UpgradeID" + "\t\t" + "ModelID");
                    foreach (var item in mus)
                    {
                        Console.WriteLine(item.ID + "\t\t" + item.UpgradeID + "\t\t" + item.ModelID);
                    }

                    break;
                case 5:
                    UniqueListMenu();
                    break;
                default:
                    break;
            }

            if (x != 0)
            {
                menuNumber = 9;
            }

            Console.WriteLine("Gombnyomás után kilép a listából");
            Console.ReadKey();
            Console.Clear();
        }

        private static void UniqueListMenu()
        {
            List<string> uniqueListMenu = new List<string>()
            {
                "Modellek ára átlagosan a választott gyárnál",
                "Legénységet igénylő modellek száma",
                "A legdrágább extrával rendelkező modell"
            };

            int menuNumber = 0;

            while (menuNumber != 9)
            {
                menuNumber = MenuNumberValidator(uniqueListMenu);

                switch (menuNumber)
                {
                    case 1:
                        AveragePrice();
                        break;
                    case 2:
                        NumberofCrewRequrieModel();
                        break;
                    case 3:
                        ModelNameWithMostExpensiveUpgrade();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void AveragePrice()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.WriteLine("Melyik gyárnak az átlag árait szeretnéd látni?");

            ListMenu(1);

            Console.WriteLine("Választott ID: ");
            int factoryID = IntBitValidator(0);

            int avgF = logic.AveragePriceofFactory(factoryID);

            Console.WriteLine("Az átlagos ár: {0}", avgF);

            Console.ReadKey();
        }

        private static void NumberofCrewRequrieModel()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.Clear();
            Console.WriteLine("Ennyi modellnél van szükség legénységre is {0}", logic.NumberofCrewRequireModel());

            Console.ReadKey();
        }

        private static void ModelNameWithMostExpensiveUpgrade()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.Clear();

            Console.WriteLine("A legdrágább extrával rendelkező modell: {0}", logic.ModelNameWithMostExpensiveUpgrade());

            Console.ReadKey();
        }

        private static void ChangeMenu(List<string> databaseList)
        {
            int menuNumber = 0;

            while (menuNumber != 9)
            {
                Console.WriteLine("Hol szeretnél változtatni?");

                menuNumber = MenuNumberValidator(databaseList);

                switch (menuNumber)
                {
                    case 1:
                        FactoryChange();
                        break;
                    case 2:
                        ModelChange();
                        break;
                    case 3:
                        UpgradeChange();
                        break;
                    case 4:
                        ModelUpgradeSwitchChange();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void FactoryChange()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.Clear();

            Console.WriteLine("Kérlek válassz egy gyárat amit módosítani szeretnél és add meg az ID-t");
            ListMenu(1);

            Console.WriteLine("Választott ID: ");
            int id = IntBitValidator(0);

            Console.WriteLine("Mit szeretnél benne változtatni?");
            Console.WriteLine(
                "1. Név" + "\r\n" +
                "2. Bolygó" + "\r\n" +
                "3. Vezető" + "\r\n" +
                "4. Oldal" + "\r\n" +
                "5. Stratégiai érték");

            int update = IntBitValidator(0);
            string input;

            switch (update)
            {
                case 1:
                    Console.WriteLine("Mi legyen az új név? ");
                    input = Console.ReadLine();
                    logic.FactoryUpdate(id, "factoryname", input);
                    break;
                case 2:
                    Console.WriteLine("Melyik bolgyón legyen? ");
                    input = Console.ReadLine();
                    logic.FactoryUpdate(id, "planet", input);
                    break;
                case 3:
                    Console.WriteLine("Új vezető? ");
                    input = Console.ReadLine();
                    logic.FactoryUpdate(id, "leader", input);
                    break;
                case 4:
                    Console.WriteLine("Új Oldal?");
                    input = Console.ReadLine();
                    logic.FactoryUpdate(id, "side", input);
                    break;
                case 5:
                    Console.WriteLine("Stratégiai érték: ");
                    int iinput = IntBitValidator(0);
                    input = iinput.ToString();
                    logic.FactoryUpdate(id, "strategicvalue", input);
                    break;
                default:
                    break;
            }
        }

        private static void ModelChange()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.Clear();

            Console.WriteLine("Kérlek válassz egy Modellt amit módosítani szeretnél és add meg az ID-t");
            ListMenu(2);

            Console.WriteLine("Választott ID: ");
            int id = IntBitValidator(0);

            Console.WriteLine("Mit szeretnél benne változtatni?");
            Console.WriteLine(
                "1. Gyártási Bolygó" + "\r\n" +
                "2. Modell neve" + "\r\n" +
                "3. Legénység szükséges" + "\r\n" +
                "4. Tűzerő" + "\r\n" +
                "5. Szín" + "\r\n" +
                "6. Ár");

            int update = IntBitValidator(0);
            string input;
            int iinput;

            switch (update)
            {
                case 1:
                    Console.WriteLine("Melyik másik bolygón készüljön? ");
                    ListMenu(1);
                    iinput = IntBitValidator(0);
                    input = iinput.ToString();
                    logic.ModelUpdate(id, "factoryid", input);
                    break;
                case 2:
                    Console.WriteLine("Modell neve: ");
                    input = Console.ReadLine();
                    logic.ModelUpdate(id, "modelname", input);
                    break;
                case 3:
                    Console.WriteLine("Legénység szükséges: ");
                    iinput = IntBitValidator(1);
                    input = iinput.ToString();
                    logic.ModelUpdate(id, "requirescrew", input);
                    break;
                case 4:
                    Console.WriteLine("Tűzerő: ");
                    iinput = IntBitValidator(0);
                    input = iinput.ToString();
                    logic.ModelUpdate(id, "firepower", input);
                    break;
                case 5:
                    Console.WriteLine("Szín: ");
                    input = Console.ReadLine();
                    logic.ModelUpdate(id, "color", input);
                    break;
                case 6:
                    Console.WriteLine("Ár:");
                    iinput = IntBitValidator(0);
                    input = iinput.ToString();
                    logic.ModelUpdate(id, "price", input);
                    break;
                default:
                    break;
            }
        }

        private static void UpgradeChange()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.Clear();

            Console.WriteLine("Kérlek válassz egy fejlesztést amit módosítani szeretnél és add meg az ID-t");
            ListMenu(3);

            Console.WriteLine("Választott ID: ");
            int id = IntBitValidator(0);

            Console.WriteLine("Mit szeretnél benne változtatni?");
            Console.WriteLine(
                "1. Fejlesztés típusa: " + "\r\n" +
                "2. Fejlesztés neve: " + "\r\n" +
                "3. Tűzerő növekedés: " + "\r\n" +
                "4. Többször használható: " + "\r\n" +
                "5.Ár");

            int update = IntBitValidator(0);
            string input;
            int iinput;

            switch (update)
            {
                case 1:
                    Console.WriteLine("Típus: ");
                    input = Console.ReadLine();
                    logic.UpgradeUpdate(id, "upgradetype", input);
                    break;
                case 2:
                    Console.WriteLine("Fejlesztés neve: ");
                    input = Console.ReadLine();
                    logic.UpgradeUpdate(id, "upgradename", input);
                    break;
                case 3:
                    Console.WriteLine("Tűzerő növekedés: ");
                    iinput = IntBitValidator(1);
                    input = iinput.ToString();
                    logic.UpgradeUpdate(id, "firepowerincrease", input);
                    break;
                case 4:
                    Console.WriteLine("Többször használható: ");
                    iinput = IntBitValidator(1);
                    input = iinput.ToString();
                    logic.UpgradeUpdate(id, "reuseable", input);
                    break;
                case 5:
                    Console.WriteLine("Ár: ");
                    iinput = IntBitValidator(1);
                    input = iinput.ToString();
                    logic.UpgradeUpdate(id, "price", input);
                    break;
                default:
                    break;
            }
        }

        private static void ModelUpgradeSwitchChange()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.Clear();

            Console.WriteLine("Kérlek válassz egy Modell-Fejlesztés kapcsolást amit módosítani szeretnél és add meg az ID-t");
            ListMenu(4);

            Console.WriteLine("Választott ID: ");
            int id = IntBitValidator(0);

            Console.WriteLine("Mit szeretnél benne változtatni?");
            Console.WriteLine(
                "1. Modell ID " + "\r\n" +
                "2. Fejlesztés ID " + "\r\n");

            int update = IntBitValidator(0);
            string input;
            int iinput;

            switch (update)
            {
                case 1:
                    Console.WriteLine("Modell ID: ");
                    iinput = IntBitValidator(1);
                    input = iinput.ToString();
                    logic.MUSUpdate(id, "upgradeid", input);
                    break;
                case 2:
                    Console.WriteLine("Fejlesztés ID: ");
                    iinput = IntBitValidator(1);
                    input = iinput.ToString();
                    logic.MUSUpdate(id, "modelid", input);
                    break;
                default:
                    break;
            }
        }

        private static void DeleteMenu(List<string> databaseList)
        {
            int menuNumber = 0;

            while (menuNumber != 9)
            {
                Console.WriteLine("Mit szeretnél törölni");

                databaseList.Add("Model - Fejlesztés kapcsolat");
                menuNumber = MenuNumberValidator(databaseList);
                databaseList.Remove("Model - Fejlesztés kapcsolat");

                switch (menuNumber)
                {
                    case 1:
                        FactoryDelete();
                        break;
                    case 2:
                        ModelDelete();
                        break;
                    case 3:
                        UpgradeDelete();
                        break;
                    case 4:
                        ModelUpgradeSwitchDelete();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void FactoryDelete()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            ListMenu(1);
            int id = IntBitValidator(0);

            var models = logic.ModelsGetAll();
            var mus = logic.MUSGetAll();

            foreach (var item in models)
            {
                if (item.FactoryID == id)
                {
                    foreach (var item2 in mus)
                    {
                        if (item2.ModelID == item.ID)
                        {
                            logic.MUSDelete((int)item2.ID);
                        }
                    }

                    logic.ModelDelete((int)item.ID);
                }
            }

            logic.FactoryDelete(id);
        }

        private static void ModelDelete()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            ListMenu(2);
            int id = IntBitValidator(0);

            var mus = logic.MUSGetAll();

            foreach (var item in mus)
            {
                if (item.ModelID == id)
                {
                    logic.MUSDelete((int)item.ID);
                }
            }

            logic.ModelDelete(id);
        }

        private static void UpgradeDelete()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.WriteLine("Melyiket szeretnéd törölni: ");
            ListMenu(3);
            int id = IntBitValidator(0);

            var mus = logic.MUSGetAll();

            foreach (var item in mus)
            {
                if (item.UpgradeID == id)
                {
                    logic.MUSDelete((int)item.ID);
                }
            }

            logic.UpgradeDelete(id);
        }

        private static void ModelUpgradeSwitchDelete()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.WriteLine("Melyiket szeretnéd törölni: ");
            ListMenu(4);
            int id = IntBitValidator(0);

            logic.MUSDelete(id);
        }

        private static void InsertMenu(List<string> databaseList)
        {
            int menuNumber = 0;

            while (menuNumber != 9)
            {
                Console.WriteLine("Hova szeretnél beszúrni?");

                databaseList.Add("Modell - Fejlesztés kapcsoló");
                menuNumber = MenuNumberValidator(databaseList);
                databaseList.Remove("Modell - Fejlesztés kapcsoló");

                switch (menuNumber)
                {
                    case 1:
                        InsertFactory();
                        break;
                    case 2:
                        InsertModel();
                        break;
                    case 3:
                        InsertUpgrade();
                        break;
                    case 4:
                        InsertMUS();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void InsertFactory()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.WriteLine("Gyár neve: ");
            string name = Console.ReadLine();
            Console.WriteLine("Bolygó: ");
            string planet = Console.ReadLine();
            Console.WriteLine("Vezető: ");
            string leader = Console.ReadLine();
            Console.WriteLine("Oldal: ");
            string side = Console.ReadLine();
            Console.WriteLine("Stratégiai fontosság: ");
            int sv = IntBitValidator(0);

            logic.FactoryCreate(name, planet, leader, side, sv);
            Console.WriteLine("A beszúrás sikeres");
        }

        private static void InsertModel()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.WriteLine("Modell neve: ");
            string modelname = Console.ReadLine();
            Console.WriteLine("Legénység szükséges 1 (igen), 0 (nem): ");
            bool rcrew = Convert.ToBoolean(IntBitValidator(1));
            Console.WriteLine("Tűzerő: ");
            int fpower = IntBitValidator(0);
            Console.WriteLine("Szín: ");
            string color = Console.ReadLine();
            Console.WriteLine("Ár/Egység: ");
            int price = IntBitValidator(0);

            Console.Clear();
            Console.WriteLine("Melyik gyárhoz szeretnéd hozzárendelni?");
            ListMenu(1);
            int factoryId = IntBitValidator(0);

            logic.ModelCreate(factoryId, modelname, rcrew, fpower, color, price);
        }

        private static void InsertUpgrade()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.WriteLine("Fejlesztés neve: ");
            string name = Console.ReadLine();
            Console.WriteLine("Fejlesztés típusa: ");
            string type = Console.ReadLine();
            Console.WriteLine("Tűzerő növekedés: ");
            int fincrease = IntBitValidator(0);
            Console.WriteLine("Többször használható");
            bool reuseable = Convert.ToBoolean(IntBitValidator(1));
            Console.WriteLine("Ár/darab: ");
            int price = IntBitValidator(0);

            logic.UpgradeCreate(type, name, fincrease, reuseable, price);
        }

        private static void InsertMUS()
        {
            IRepository repository = new Repository();
            Logic logic = new Logic(repository);

            Console.WriteLine("Kérlek válassz egy modellt: ");
            ListMenu(2);
            int mid = IntBitValidator(0);
            Console.WriteLine("Kérlek válassz egy fejlesztést: ");
            ListMenu(3);
            int uid = IntBitValidator(0);
            logic.MUSCreate(uid, mid);
        }

        private static void Bid()
        {
            Console.WriteLine("Add meg egy modell nevét");
            string modelName = Console.ReadLine();
            Console.WriteLine("Add meg az irányárat");
            string price = IntBitValidator(0).ToString();

            XDocument xdoc = XDocument.Load(@"http://localhost:8080/OENIK_PROG3_2019_1_X7IKPT/OffersServlet?modelname=" + modelName + "&price=" + price);

            Console.WriteLine("Megrendelő neve | Modell neve | Ajánlott ár");

            foreach (var item in xdoc.Element("generatedOffers").Elements("offers"))
            {
                Console.WriteLine(item.Element("buyername").Value + "----" + item.Element("modelname").Value + "----" + item.Element("price").Value);
            }

            Console.ReadLine();
        }

        private static int IntBitValidator(int x)
        {
            bool siker = false;

            if (x == 1)
            {
                while (siker == false)
                {
                    Console.WriteLine("Kérlek 0 (hamis), 1 (igaz) értéket adj meg: ");
                    if (int.TryParse(Console.ReadLine(), out int result) && (result == 1 || result == 0))
                    {
                        siker = true;
                        return result;
                    }
                }
            }
            else
            {
                while (siker == false)
                {
                    Console.WriteLine("Kérlek adj meg egy számot: ");
                    if (int.TryParse(Console.ReadLine(), out int result))
                    {
                        siker = true;
                        return result;
                    }
                }
            }

            return 1;
        }

        private static int MenuNumberValidator(List<string> menu)
        {
            bool sikeres = false;

            while (!sikeres)
            {
                Console.Clear();
                Console.WriteLine("Kérlek válassz az alábbi menüpontok közül: ");

                for (int i = 0; i < menu.Count; i++)
                {
                    Console.WriteLine("{0}. {1}", i + 1, menu[i]);
                }

                Console.WriteLine("9. Kilépés");

                string input = Console.ReadLine();

                if (int.TryParse(input, out int inputInt))
                {
                    if ((inputInt > 0 && inputInt < menu.Count + 1) || inputInt == 9)
                    {
                        sikeres = true;
                        return inputInt;
                    }
                    else
                    {
                        Console.WriteLine("Nincs ilyen menüpont. Gombnyomás után kérlek próbáld újra");
                        Console.ReadKey();
                    }
                }
                else
                {
                    Console.WriteLine("Kérlek egy számot adj meg. Gombnyomás után kérlek próbáld újra");
                    Console.ReadKey();
                }
            }

            return 9;
        }
    }
}