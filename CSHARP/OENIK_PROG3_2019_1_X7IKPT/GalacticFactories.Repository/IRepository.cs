﻿// Enable XML documentation output
// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GalacticFactories.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalacticFactories.Data;

    /// <summary>
    /// Irepository interface
    /// </summary>
    public interface IRepository
    {
        Factories GetOneFactory(int id);
        bool ChangeFactory(int id, Factories newFactory, string newName, string newPlanet, string newLeader, string newSide, int newStratValue);

        /// <summary>
        /// Factory get method
        /// </summary>
        /// <returns>Factories</returns>
        IEnumerable<Factories> FactoriesGetAll();

        /// <summary>
        /// Factory create method
        /// </summary>
        /// <param name="newFactory">New factory class</param>
        void FactoryCreate(Factories newFactory);

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="id">update id</param>
        /// <param name="update">field name</param>
        /// <param name="change">new update</param>
        void FactoryUpdate(int id, string update, string change);

        /// <summary>
        /// Delete method
        /// </summary>
        /// <param name="id">Delete factory</param>
        void FactoryDelete(int id);

        /// <summary>
        /// Get method
        /// </summary>
        /// <returns>Models</returns>
        IEnumerable<Models> ModelsGetAll();

        /// <summary>
        /// Create method
        /// </summary>
        /// <param name="newModel">Model</param>
        void ModelCreate(Models newModel);

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="id">update id</param>
        /// <param name="update">field name</param>
        /// <param name="change">new data</param>
        void ModelUpdate(int id, string update, string change);

        /// <summary>
        /// Delete method
        /// </summary>
        /// <param name="id">Delete id</param>
        void ModelDelete(int id);

        /// <summary>
        /// Get Method
        /// </summary>
        /// <returns>Upgrades</returns>
        IEnumerable<Upgrades> UpgradesGetAll();

        /// <summary>
        /// Upgrade create
        /// </summary>
        /// <param name="newUpgrade">Upgrade object</param>
        void UpgradeCreate(Upgrades newUpgrade);

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="id">upgrade id</param>
        /// <param name="update">upgrade field</param>
        /// <param name="change">new data</param>
        void UpgradeUpdate(int id, string update, string change);

        /// <summary>
        /// Delete method
        /// </summary>
        /// <param name="id">upgrade id</param>
        void UpgradeDelete(int id);

        /// <summary>
        /// Get method
        /// </summary>
        /// <returns>Model update Switch</returns>
        IEnumerable<ModelUpgradeSwitch> MUSGetAll();

        /// <summary>
        /// Create method
        /// </summary>
        /// <param name="newMUS">Mus class</param>
        void MUSCreate(ModelUpgradeSwitch newMUS);

        /// <summary>
        /// Model upgrade Switch update
        /// </summary>
        /// <param name="id">upgrade id</param>
        /// <param name="update">upgrade field</param>
        /// <param name="change">new data</param>
        void MUSUpdate(int id, string update, string change);

        /// <summary>
        /// Delete method
        /// </summary>
        /// <param name="id">delete id</param>
        void MUSDelete(int id);
    }
}
