﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GalacticFactories.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalacticFactories.Data;

    /// <summary>
    /// Repository
    /// </summary>
    public class Repository : IRepository, IDisposable
    {
        private GalacticFactoriesEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        public Repository()
        {
            this.entities = new GalacticFactoriesEntities();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="entities">Entity model</param>
        public Repository(GalacticFactoriesEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Dispose Method
        /// </summary>
        public void Dispose()
        {
            this.entities.Dispose();
        }

        /// <summary>
        /// Get method
        /// </summary>
        /// <returns>Factories</returns>
        public IEnumerable<Factories> FactoriesGetAll()
        {
            return this.entities.Factories;
        }

        public Factories GetOneFactory(int id)
        {
            return FactoriesGetAll().SingleOrDefault(x => x.ID == id);
        }

        /// <summary>
        /// Create method
        /// </summary>
        /// <param name="newFactory">Factory create</param>
        public void FactoryCreate(Factories newFactory)
        {
            this.entities.Factories.Add(newFactory);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Delete method
        /// </summary>
        /// <param name="id">Delete factory</param>
        public void FactoryDelete(int id)
        {
            GalacticFactoriesEntities factoryEntities = new GalacticFactoriesEntities();

            Factories d = factoryEntities.Factories.Single(x => x.ID == id);

            var models = ModelsGetAll();
            var mus = MUSGetAll();

            foreach (var item in models)
            {
                if (item.FactoryID == id)
                {
                    foreach (var item2 in mus)
                    {
                        if (item2.ModelID == item.ID)
                        {
                            MUSDelete((int)item2.ID);
                        }
                    }

                    ModelDelete((int)item.ID);
                }
            }

            factoryEntities.Factories.Remove(d);

            factoryEntities.SaveChanges();
        }

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="id">update id</param>
        /// <param name="update">field name</param>
        /// <param name="change">new update</param>
        public void FactoryUpdate(int id, string update, string change)
        {
            Factories f = this.entities.Factories.Single(x => x.ID == id);

            switch (update.ToUpper())
            {
                case "ID":
                    f.ID = int.Parse(change);
                    break;
                case "FACTORYNAME":
                    f.FactoryName = change;
                    break;
                case "PLANET":
                    f.Planet = change;
                    break;
                case "LEADER":
                    f.Leader = change;
                    break;
                case "SIDE":
                    f.Side = change;
                    break;
                case "STRATEGICVALUE":
                    f.StrategicValue = int.Parse(change);
                    break;
                default:
                    break;
            }

            this.entities.SaveChanges();
        }

        public bool ChangeFactory(int id, Factories newFactory, string newName, string newPlanet, string newLeader, string newSide, int newStratValue)
        {
            Factories entity = GetOneFactory(id);
            if (entity == null)
            {
                return false;
            }

            entity.FactoryName = newName;
            entity.Planet = newPlanet;
            entity.Leader = newLeader;
            entity.Side = newSide;
            entity.StrategicValue = newStratValue;
            this.entities.SaveChanges();
            return true;
        }

        /// <summary>
        /// Get method
        /// </summary>
        /// <returns>Models</returns>
        public IEnumerable<Models> ModelsGetAll()
        {
            return this.entities.Models;
        }

        /// <summary>
        /// Create method
        /// </summary>
        /// <param name="newModel">Model</param>
        public void ModelCreate(Models newModel)
        {
            this.entities.Models.Add(newModel);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Delete method
        /// </summary>
        /// <param name="id">Delete id</param>
        public void ModelDelete(int id)
        {
            GalacticFactoriesEntities modelEntities = new GalacticFactoriesEntities();

            Models d = modelEntities.Models.Single(x => x.ID == id);

            modelEntities.Models.Remove(d);

            modelEntities.SaveChanges();
        }

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="id">update id</param>
        /// <param name="update">field name</param>
        /// <param name="change">new data</param>
        public void ModelUpdate(int id, string update, string change)
        {
            Models m = this.entities.Models.Single(x => x.ID == id);

            switch (update.ToUpper())
            {
                case "ID":
                    m.ID = int.Parse(change);
                    break;
                case "FACTORYID":
                    m.FactoryID = int.Parse(change);
                    break;
                case "MODELNAME":
                    m.ModelName = change;
                    break;
                case "REQUIRESCREW":
                    m.RequiresCrew = bool.Parse(change);
                    break;
                case "FIREPOWER":
                    m.FirePower = int.Parse(change);
                    break;
                case "COLOR":
                    m.Color = change;
                    break;
                case "PRICE":
                    m.Price = int.Parse(change);
                    break;
                default:
                    break;
            }

            this.entities.SaveChanges();
        }

        /// <summary>
        /// Get method
        /// </summary>
        /// <returns>Model update Switch</returns>
        public IEnumerable<ModelUpgradeSwitch> MUSGetAll()
        {
            return this.entities.ModelUpgradeSwitch;
        }

        /// <summary>
        /// Create method
        /// </summary>
        /// <param name="newMUS">Mus class</param>
        public void MUSCreate(ModelUpgradeSwitch newMUS)
        {
            this.entities.ModelUpgradeSwitch.Add(newMUS);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Delete method
        /// </summary>
        /// <param name="id">delete id</param>
        public void MUSDelete(int id)
        {
            GalacticFactoriesEntities mUSEntities = new GalacticFactoriesEntities();

            ModelUpgradeSwitch d = mUSEntities.ModelUpgradeSwitch.Single(x => x.ID == id);
            mUSEntities.ModelUpgradeSwitch.Remove(d);
            mUSEntities.SaveChanges();
        }

        /// <summary>
        /// Model upgrade Switch update
        /// </summary>
        /// <param name="id">upgrade id</param>
        /// <param name="update">upgrade field</param>
        /// <param name="change">new data</param>
        public void MUSUpdate(int id, string update, string change)
        {
            ModelUpgradeSwitch m = this.entities.ModelUpgradeSwitch.Single(x => x.ID == id);

            switch (update.ToUpper())
            {
                case "ID":
                    m.ID = int.Parse(change);
                    break;
                case "UPGRADEID":
                    m.UpgradeID = int.Parse(change);
                    break;
                case "MODELID":
                    m.ModelID = int.Parse(change);
                    break;
                default:
                    break;
            }

            this.entities.SaveChanges();
        }

        /// <summary>
        /// Get Method
        /// </summary>
        /// <returns>Upgrades</returns>
        public IEnumerable<Upgrades> UpgradesGetAll()
        {
            return this.entities.Upgrades;
        }

        /// <summary>
        /// Upgrade create
        /// </summary>
        /// <param name="newUpgrade">Upgrade object</param>
        public void UpgradeCreate(Upgrades newUpgrade)
        {
            this.entities.Upgrades.Add(newUpgrade);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Delete method
        /// </summary>
        /// <param name="id">upgrade id</param>
        public void UpgradeDelete(int id)
        {
            GalacticFactoriesEntities upgradeEntities = new GalacticFactoriesEntities();

            Upgrades d = upgradeEntities.Upgrades.Single(x => x.ID == id);
            upgradeEntities.Upgrades.Remove(d);
            upgradeEntities.SaveChanges();
        }

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="id">upgrade id</param>
        /// <param name="update">upgrade field</param>
        /// <param name="change">new data</param>
        public void UpgradeUpdate(int id, string update, string change)
        {
            Upgrades u = this.entities.Upgrades.Single(x => x.ID == id);

            switch (update.ToUpper())
            {
                case "ID":
                    u.ID = int.Parse(change);
                break;
                case "UPGRADETYPE":
                    u.UpgradeType = change;
                break;
                case "UPGRADENAME":
                    u.UpgradeName = change;
                break;
                case "FIREPOWERINCREASE":
                    u.FirePowerIncrease = int.Parse(change);
                    break;
                case "REUSEABLE":
                    u.Reuseable = bool.Parse(change);
                    break;
                case "PRICE":
                    u.Price = int.Parse(change);
                    break;
                default:
                    break;
            }
        }
    }
}
