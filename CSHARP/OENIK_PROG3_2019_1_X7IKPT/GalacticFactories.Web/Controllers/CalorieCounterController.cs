﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GalacticFactories.Web.Models;

namespace GalacticFactories.Web.Controllers
{
    public class CalorieCounterController : Controller
    {
        public static List<Exercise> exercises = new List<Exercise>()
        {
            new Exercise("Running", 1000),
            new Exercise("Yoga", 400),
            new Exercise("Pilates", 472),
            new Exercise("Hiking", 700),
            new Exercise("Swimming", 1000),
            new Exercise("Bicycle", 600)
        };
        public ActionResult Calories()
        {
            return View("CaloriesInput", exercises);
        }

        [HttpPost]
        public ActionResult Calories(CaloriesInput input)
        {
            if (!Request.Form.AllKeys.Contains(nameof(CaloriesInput.Lenght)) ||
                !Request.Form.AllKeys.Contains(nameof(CaloriesInput.Weight)) ||
                !Request.Form.AllKeys.Contains(nameof(CaloriesInput.Name))
                )
            {
                TempData["myWarning"] = "MISSING DATA";
                return this.RedirectToAction(nameof(Calories));
            }

            if (input.Lenght < 0 || input.Weight < 0)
            {
                TempData["myWarning"] = "INVALID DATA";
                return this.RedirectToAction(nameof(Calories));
            }

            CaloriesResult cr = new CaloriesResult()
            {
                Name = input.Name,
                Weight = input.Weight,
                Length = input.Lenght,
                BurnedCalories = BurnedCaloriesCalculator(input.Exercise, input.Lenght, input.Weight)
            };

            return View("CaloriesResult", cr);
        }

        public double BurnedCaloriesCalculator(string exname, double time, double weight)
        {
            double result = 0;

            for (int i = 0; i < exercises.Count; i++)
            {
                if (exercises[i].ExerciseName == exname)
                {
                    result = exercises[i].Burn * (time / 60) * (weight / 100);
                }
            }

            return result;
        }
    }
}