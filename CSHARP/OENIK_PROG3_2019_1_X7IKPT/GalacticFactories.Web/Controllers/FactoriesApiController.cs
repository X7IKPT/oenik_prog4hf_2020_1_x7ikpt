﻿using AutoMapper;
using GalacticFactories.Data;
using GalacticFactories.Logic;
using GalacticFactories.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GalacticFactories.Web.Controllers
{
    public class FactoriesApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic logic;
        IMapper mapper;
        FactoryViewModel model;

        public FactoriesApiController()
        {
            GalacticFactoriesEntities ctx = new GalacticFactoriesEntities();
            Repository.Repository repo = new Repository.Repository(ctx);
            logic = new Logic.Logic(repo);
            mapper = MapperFactory.CreateMapper();
            model = new FactoryViewModel();
            model.EditFactory = new Factory();
        }

        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Factory> GetAll()
        {
            var factories = logic.FactoriesGetAll();
            return mapper.Map<IEnumerable<Data.Factories>, List<Models.Factory>>(factories);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneFactory(int id)
        {
            return new ApiResult() { OperationResult = logic.DeleteFactory(id) };
        }

        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneFactory( Factory factory)
        {
            logic.FactoryCreate(factory.FactoryName, factory.Planet, factory.Leader, factory.Side, factory.StrategicValue);
            return new ApiResult() { OperationResult = true };
        }

        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneFactory(Factory factory)
        {
            return new ApiResult()
            {
                OperationResult = logic.ChangeFactory(factory.Id, factory.FactoryName,factory.Planet,factory.Leader,factory.Side,factory.StrategicValue)
            };
        }
    }
}
