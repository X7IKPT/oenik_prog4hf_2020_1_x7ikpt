﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using GalacticFactories.Data;
using GalacticFactories.Repository;
using GalacticFactories.Logic;
using GalacticFactories.Web.Models;

namespace GalacticFactories.Web.Controllers
{
    public class FactoryController : Controller
    {
        ILogic logic;
        IMapper mapper;
        FactoryViewModel model;
        
        public FactoryController()
        {
            GalacticFactoriesEntities ctx = new GalacticFactoriesEntities();
            Repository.Repository repo = new Repository.Repository(ctx);
            logic = new Logic.Logic(repo);
            mapper = MapperFactory.CreateMapper();
            model = new FactoryViewModel();
            model.EditFactory = new Factory();

            var factories = logic.FactoriesGetAll();
            model.ListOfFactories = mapper.Map<IEnumerable<Data.Factories>, List<Models.Factory>>(factories);
        }

        private Factory GetFactoryModel(int id)
        {
            Factories oneFactory = logic.GetOneFactory(id);
            return mapper.Map<Factories, Factory>(oneFactory);
        }

        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("FactoryIndex", model);
        }

        // GET: Factory/Details/5
        public ActionResult Details(int id)
        {
            return View("FactoryDetails", GetFactoryModel(id));
        }

        public ActionResult Remove(int id)
        {
            logic.FactoryDelete(id);
            TempData["editResult"] = "Delete OK";
            return RedirectToAction("Index");
        }

        // GET: Factory/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            model.EditFactory = GetFactoryModel(id);
            return View("FactoryIndex", model);
        }

        // POST: Factory/Edit/5
        [HttpPost]
        public ActionResult Edit(Factory factory, string editAction)
        {
            if (ModelState.IsValid && factory != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.FactoryCreate(factory.FactoryName, factory.Planet, factory.Leader, factory.Side, factory.StrategicValue);
                }
                else
                {
                    if (!logic.ChangeFactory(factory.Id, factory.FactoryName, factory.Planet, factory.Leader, factory.Side, factory.StrategicValue))
                    {
                        TempData["editResult"] = "Edit Fail";
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                model.EditFactory = factory;
                return View("FactoryIndex", model);
            }
        }
    }
}
