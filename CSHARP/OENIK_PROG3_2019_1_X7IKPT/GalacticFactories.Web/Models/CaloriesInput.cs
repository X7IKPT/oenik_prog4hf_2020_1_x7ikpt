﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalacticFactories.Web.Models
{
    public class CaloriesInput
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public int Lenght { get; set; }
        public string Exercise { get; set; }
    }
}