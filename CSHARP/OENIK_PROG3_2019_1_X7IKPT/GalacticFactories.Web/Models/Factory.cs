﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GalacticFactories.Web.Models
{
    public class Factory
    {
        [Display(Name = "Factory id")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Factory Name")]
        [Required]
        [StringLength(30, MinimumLength = 3)]
        public string FactoryName { get; set; }

        [Display(Name = "Factory Planet")]
        [Required]
        public string Planet { get; set; }

        [Display(Name = "Factory Leader")]
        [Required]
        public string Leader { get; set; }

        [Display(Name = "Factory Side")]
        [Required]
        public string Side { get; set; }

        [Display(Name = "Factory Strategic Value")]
        [Required]
        public int StrategicValue { get; set; }
    }
}