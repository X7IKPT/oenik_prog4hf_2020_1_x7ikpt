﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GalacticFactories.Web.Models
{
    public class FactoryViewModel
    {
        public Factory EditFactory { get; set; }
        public List<Factory> ListOfFactories { get; set; }
    }
}