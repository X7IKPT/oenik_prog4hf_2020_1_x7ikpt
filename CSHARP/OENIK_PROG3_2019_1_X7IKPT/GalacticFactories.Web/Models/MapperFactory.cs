﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace GalacticFactories.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GalacticFactories.Data.Factories, GalacticFactories.Web.Models.Factory>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.ID)).
                ForMember(dest => dest.FactoryName, map => map.MapFrom(src => src.FactoryName)).
                ForMember(dest => dest.Planet, map => map.MapFrom(src => src.Planet)).
                ForMember(dest => dest.Leader, map => map.MapFrom(src => src.Leader)).
                ForMember(dest => dest.Side, map => map.MapFrom(src => src.Side)).
                ForMember(dest => dest.StrategicValue, map => map.MapFrom(src => src.StrategicValue));
            });
            return config.CreateMapper();
        }
    }
}