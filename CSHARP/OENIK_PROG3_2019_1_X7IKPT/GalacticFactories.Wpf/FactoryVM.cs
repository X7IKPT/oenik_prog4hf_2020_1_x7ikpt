﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace GalacticFactories.Wpf
{
    class FactoryVM : ObservableObject
    {
		private int id;
		private string factoryName;
		private string planet;
		private string leader;
		private string side;
		private int strategicValue;

		public int StrategicValue
		{
			get { return strategicValue; }
			set { Set(ref strategicValue, value); }
		}

		public string Side
		{
			get { return side; }
			set { Set(ref side, value); }
		}

		public string Leader
		{
			get { return leader; }
			set { Set(ref leader, value); }
		}

		public string Planet
		{
			get { return planet; }
			set { Set(ref planet, value); }
		}

		public string FactoryName
		{
			get { return factoryName; }
			set { Set(ref factoryName, value); }
		}

		public int Id
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		public void CopyFrom(FactoryVM other)
		{
			if (other == null)
			{
				return;
			}

			this.Id = other.Id;
			this.FactoryName = other.FactoryName;
			this.Planet = other.Planet;
			this.Leader = other.Leader;
			this.Side = other.Side;
			this.StrategicValue = other.StrategicValue;
		}
	}
}
