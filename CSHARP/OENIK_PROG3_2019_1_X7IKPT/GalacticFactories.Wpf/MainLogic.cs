﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GalacticFactories.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:53327/api/FactoriesApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "FactoryResult");
        }

        public List<FactoryVM> ApiGetFactories()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<FactoryVM>>(json);
            return list;
        }

        public void ApiDelFactory(FactoryVM factory)
        {
            bool success = false;
            if (factory != null)
            {
                string json = client.GetStringAsync(url + "del/" + factory.Id.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditFactory(FactoryVM factory, bool isEditing)
        {
            if (factory == null)
            {
                return false;
            }

            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(FactoryVM.Id), factory.Id.ToString());
            }

            postData.Add(nameof(FactoryVM.FactoryName), factory.FactoryName);
            postData.Add(nameof(FactoryVM.Planet), factory.Planet);
            postData.Add(nameof(FactoryVM.Leader), factory.Leader);
            postData.Add(nameof(FactoryVM.Side), factory.Side);
            postData.Add(nameof(FactoryVM.StrategicValue), factory.StrategicValue.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditFactory(FactoryVM factory, Func<FactoryVM, bool> editor)
        {
            FactoryVM clone = new FactoryVM();

            if (factory != null)
            {
                clone.CopyFrom(factory);
            }

            bool? success = editor?.Invoke(clone);

            if (success == true)
            {
                if (factory != null)
                {
                    success = ApiEditFactory(clone, true);
                }
                else
                {
                    success = ApiEditFactory(clone, false);
                }
            }
            SendMessage(success == true);
        }
    }
}
