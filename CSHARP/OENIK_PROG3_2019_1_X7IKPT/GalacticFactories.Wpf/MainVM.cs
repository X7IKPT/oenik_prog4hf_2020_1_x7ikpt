﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;

namespace GalacticFactories.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private ObservableCollection<FactoryVM> allFactories;
        private FactoryVM selectedFactory;

        public FactoryVM SelectedFactory
        {
            get { return selectedFactory; }
            set { Set(ref selectedFactory, value); }
        }

        public ObservableCollection<FactoryVM> AllFactories
        {
            get { return allFactories; }
            set { Set(ref allFactories, value); }
        }

        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<FactoryVM, bool> EditorFunc { get; set; }

        public MainVM()
        {
            logic = new MainLogic();

            LoadCmd = new RelayCommand(() => AllFactories = new ObservableCollection<FactoryVM>(logic.ApiGetFactories()));
            DelCmd = new RelayCommand(() => logic.ApiDelFactory(selectedFactory));
            AddCmd = new RelayCommand(() => logic.EditFactory(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditFactory(selectedFactory, EditorFunc));
        }
    }
}
