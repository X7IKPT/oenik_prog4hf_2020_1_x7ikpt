﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Something.Web.Models
{
    public class CaloriesResult
    {
        public string Name { get; set; }
        public double Weight { get; set; }
        public int Length { get; set; }
        public double BurnedCalories { get; set; }
    }
}