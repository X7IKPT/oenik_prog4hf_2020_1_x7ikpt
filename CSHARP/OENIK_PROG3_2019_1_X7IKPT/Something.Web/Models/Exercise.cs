﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Something.Web.Models
{
    public class Exercise
    {
        public string ExerciseName { get; set; }
        public int Burn { get; set; }

        public Exercise(string ename, int burn)
        {
            this.Burn = burn;
            this.ExerciseName = ename;
        }
    }
}