package Controller;

import Model.OfferGenerator;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class GeneratedOffers {
    @XmlElement
    List<OfferGenerator> offers;
    
    public GeneratedOffers(){
        
    }
    
    public GeneratedOffers(String modelName, int price){
        this.offers = Generate(modelName, price);
    }
    
    private List<OfferGenerator> Generate(String modelName, int price){
        ArrayList<OfferGenerator> tempOffers = new ArrayList<>();
        
        for (int i = 0; i < 5; i++) {
            tempOffers.add(new OfferGenerator(modelName, price));                    
        }
        
        return tempOffers;
    }
}
