package Model;

import java.util.Random;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OfferGenerator {
    private String modelName;
    private String buyerName;
    private int price;
    
    String[] possibleNames = {"Vader", "Luke", "Leia", "HanSolo", "Chewbacca"};
    
    public OfferGenerator(){
        
    }
    
    public OfferGenerator(String modelName, int price){
        Random rn = new Random();
        this.modelName = modelName;
        this.buyerName = possibleNames[rn.nextInt(4)];
        this.price = price + rn.nextInt(price) - rn.nextInt(price);
    }
    
    @XmlElement(name="modelname")
    public String getModelName(){
        return modelName;
    }
    
    @XmlElement(name="buyername")
    public String getBuyerName(){
        return buyerName;
    }
    
    @XmlElement(name="price")
    public int getPrice(){
        return price;
    }
}
